#+title: README - parish website
#+author: David
#+date: <2018-05-22 Tue>

* About

The St Joseph's (Newton Abbot) parish website is built from a
repository hosted on Gitlab. It is made as org-mode files which are
then turned into the website proper via a GitLab facility which runs
GNU Emacs to convert the org files to HTML.

If you have a GitLab account (or create one) you can request/suggest
changes or corrections to these pages and, if they are approved by the
priests, they will appear on the live site. 

Published on gitlab pages at
https://ppymdjr.gitlab.io/parish-website/home.html and at
http://st.josephs.org.uk (which is the official address).

* Todo

1. Add other relevant pages from previous site
2. In particular get the groups page back
   1. I need to auto generate the links page. Maybe likewise for the top too
3. License?
4. Optimize images. If I want to publish bigger images and files etc I
   might put them in keybase.
5. Put in rewrites from the previous page names
6. SSL termination via CloudFlare?

* Testing/building locally

I think it would make sense to try and use docker to build this. It
should give the same results as what gitlab will do. I need a little
time to do that though.

I think I just need to start with iquiw/alpine-emacs, copy all the
things into it from this directory, run the emacs script and then pull
things out. That should be easy enough. Probably needs a docker
volume.

I have disabled the package initialization here in publish.el, which
makes it much quicker.

The following just runs the emacs from the image:-
~docker run -it iquiw/alpine-emacs~

In the meantime I have a simple build script which does the job for
me - see build.sh, but it uses the Emacs.app from my machine and isn't
really that great.

https://thibaultmarin.github.io/blog/posts/2016-11-13-Personal_website_in_org.html#org3ae0edc

An nginx configuration:-
#+BEGIN_SRC sh
upstream gitlab {
    server ppymdjr.gitlab.io:443;
}

server {
       server_name st.josephs.org.uk;
       location / {
           proxy_set_header Host ppymdjr.gitlab.io;
           proxy_pass https://gitlab/parish-website/;
       }
       location = / {
           return 301 /home.html;
       }

}

#+END_SRC


* Microsoft Publisher File Conversion
[[https://www.publishertopdf.com/][This seems to work well]] for turning .pub files into PDFs
